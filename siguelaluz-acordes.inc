\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t

		% intro
		g1 g1

		% dios te ha escogido hoy...
		g1 g1 fis1:m fis1:m
		e1:m e1:m c1 c1
		a1:m a1:m
		d1 d1

		% dios te ha escogido hoy...
		g1 g1 fis1:m fis1:m
		e1:m e1:m c1 c1
		a1:m a1:m
		d1 d1:7

		% no pienses en el mannana...
		g1 g1 b1:7 b1:7
		e1:m e1:m
		c1 c1
		a1:m a1:7
		d1 d1:7		

		% llevalo contigo siempre...
		g1 g1 b1:7 b1:7
		e1:m e1:m
		c1 c1
		a1:m a1:7
		d1 d1:7		

		% sigue adelante fiel...
		g1 g1 fis1:m fis1:m
		e1:m e1:m c1 c1
		a1:m a1:m
		d1 d1


		% dios te ha escogido hoy...
		g1 g1 fis1:m fis1:m
		e1:m e1:m c1 c1
		a1:m a1:m
		d1 d1:7

		% no pienses en el mannana...
		g1 g1 b1:7 b1:7
		e1:m e1:m
		c1 c1
		a1:m a1:7
		d1 d1:7		

		% llevalo contigo siempre...
		g1 g1 b1:7 b1:7
		e1:m e1:m
		c1 c1
		a1:m a1:7
		d1 d1:7		

		% sigue la luz...
		g1 g1
	}
