\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key g \major

		R1*2  |
		r4 d' 8 d' 4 d' e' 8 ~  |
		e' 8 d' 4 d' 8 ( ~ d' 2  |
%% 5
		cis' 1 ~  |
		cis' 1 )  |
		r4 b 8 b 4 b cis' 8 ~  |
		cis' 8 b 4 b 8 ~ b 2  |
		r4 c' 8 c' 4 c' d' 8 ~  |
%% 10
		d' 8 c' 4 c' 8 ~ c' 2  |
		r4 c' 8 c' 4 c' d' 8 ~  |
		d' 8 c' 4 b ( c' 4. )  |
		d' 1 ~  |
		d' 1  |
%% 15
		r4 d' 8 d' 4 d' e' 8 ~  |
		e' 8 d' 4 d' 8 ( ~ d' 2  |
		cis' 1 ~  |
		cis' 1 )  |
		r4 b 8 b 4 b cis' 8 ~  |
%% 20
		cis' 8 b 4 b 8 ~ b 2  |
		r4 c' 8 c' 4 c' d' 8 ~  |
		d' 8 c' 4 c' 8 ~ c' 2  |
		r4 c' 8 c' 4 c' d' 8 ~  |
		d' 8 c' 4 b ( c' 4. )  |
%% 25
		d' 1  |
		r8 g' g' g' g' g' g' g' ~  |
		g' 4. g' 8 ~ g' 2 ~  |
		g' 8 r r g' 4 fis' 8 g' 4  |
		fis' 1 ~  |
%% 30
		fis' 8 r r fis' 4 ( e' 8 fis' ) e'  |
		e' 4. b 8 ~ b 2  |
		r8 b b b b b 4 b 8  |
		c' 4 c' 8 c' ~ c' 2 ~  |
		c' 2 r8 e' e' e'  |
%% 35
		e' 4. c' 8 ~ c' 4. r8  |
		r8 d' e' d' 16 e' 8. d' 8 e' d' ~  |
		d' 2.. r8  |
		r8 g' 4 g' 8 g' g' g' g'  |
		g' 4. g' 8 ~ g' 2 ~  |
%% 40
		g' 8 r r g' g' 8. fis' g' 8 ~  |
		g' 8 fis' 2. r8  |
		r8 e' fis' e' fis' e' ( fis' 4 )  |
		e' 4. b 8 ~ b 2  |
		r8 b b b b b b c' ~  |
%% 45
		c' 1 ~  |
		c' 4. r8 r e' e' e'  |
		e' 4. c' 8 ~ c' 2  |
		r8 d' e' d' 16 e' 8. d' 8 e' d' ~  |
		d' 1 ~  |
%% 50
		d' 2 r  |
		r4 d' 8 d' 4 d' e' 8 ~  |
		e' 8 d' 4 d' 8 ~ d' 2  |
		r4 cis' 8 cis' cis' b 4 cis' 8 ~  |
		cis' 1  |
%% 55
		r4 b 8 b 4 b cis' 8 ~  |
		cis' 8 b 4 b 8 ~ b 2  |
		r4 c' 8 c' 4 c' d' 8 ~  |
		d' 8 c' 4 c' 8 ~ c' 2  |
		r4 c' 8 c' 4 c' d' 8 ~  |
%% 60
		d' 8 c' 4 b c' 4.  |
		d' 1 ~  |
		d' 1  |
		r4 d' 8 d' 4 d' e' 8 ~  |
		e' 8 d' 4 d' 8 ( ~ d' 2  |
%% 65
		cis' 1 ~  |
		cis' 1 )  |
		r4 b 8 b 4 b cis' 8 ~  |
		cis' 8 b 4 b 8 ~ b 2  |
		r4 c' 8 c' 4 c' d' 8 ~  |
%% 70
		d' 8 c' 4 c' 8 ~ c' 2  |
		r4 c' 8 c' 4 c' d' 8 ~  |
		d' 8 c' 4 b ( c' 4. )  |
		d' 1  |
		r8 g' g' g' g' g' g' g' ~  |
%% 75
		g' 4. g' 8 ~ g' 2 ~  |
		g' 8 r r g' 4 fis' 8 g' 4  |
		fis' 1 ~  |
		fis' 8 r r fis' 4 ( e' 8 fis' ) e'  |
		e' 4. b 8 ~ b 2  |
%% 80
		r8 b b b b b 4 b 8  |
		c' 4 c' 8 c' ~ c' 2 ~  |
		c' 2 r8 e' e' e'  |
		e' 4. c' 8 ~ c' 4. r8  |
		r8 d' e' d' 16 e' 8. d' 8 e' d' ~  |
%% 85
		d' 2.. r8  |
		r8 g' 4 g' 8 g' g' g' g'  |
		g' 4. g' 8 ~ g' 2 ~  |
		g' 8 r r g' g' 8. fis' g' 8 ~  |
		g' 8 fis' 2. r8  |
%% 90
		r8 e' fis' e' fis' e' ( fis' 4 )  |
		e' 4. b 8 ~ b 2  |
		r8 b b b b b b c' ~  |
		c' 1 ~  |
		c' 4. r8 r e' e' e'  |
%% 95
		e' 4. c' 8 ~ c' 2  |
		r8 d' e' d' 16 e' 8. d' 8 e' d' ~  |
		d' 1 ~  |
		d' 8 r r4 d' 8 c' b b ~  |
		b 1  |
%% 100
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		Dios "te ha es" -- co -- gi -- do hoy, __
		%tie -- nes que par -- tir. __
		Dios "te ha es" -- co -- gi -- do hoy, __
		de -- ja tu ca -- "sa y" ven. __
		Dios "te ha es" -- co -- gi -- do hoy, ven. __

		Dios "te ha es" -- co -- gi -- do hoy, __
		%tie -- nes que llo -- rar. __
		Dios "te ha es" -- co -- gi -- do hoy, __
		tam -- bién de -- bes re -- ír. __
		Dios "te ha es" -- co -- gi -- do hoy, ven.

		No pien -- ses en el ma -- ña -- na, __
		dé -- ja -- "lo es" -- tar. __
		Ca -- da dí -- a __
		tie -- ne bas -- tan -- te con "su in" -- quie -- tud, __
		vi -- ve tu vi -- da, __
		siem -- pre mi -- ran -- "do a" la luz. __

		Llé -- va -- la con -- ti -- go siem -- pre, __
		no mi -- res a -- trás.
		Y cuan -- "do a" ve -- ces sien -- tas __
		nos -- tal -- gia "y os" -- cu -- ri -- dad, __
		pien -- sa que al -- guien __
		tie -- ne tu mis -- "mo i" -- de -- al. __

		Si -- "gue a" -- de -- lan -- te fiel, __
		a ve -- ces cae -- rás. __
		Lu -- cha con gran te -- són, __
		vuél -- ve -- "te a" le -- van -- tar, __
		lu -- "cha en" -- car -- ni -- za -- da ten -- drás. __

		Dios "te ha es" -- co -- gi -- do hoy, __ 
		%tie -- nes que par -- tir. __
		Dios "te ha es" -- co -- gi -- do hoy, __
		de -- ja tu ca -- "sa y" ven. __
		Dios "te ha es" -- co -- gi -- do hoy, ven.

		No pien -- ses en el ma -- ña -- na, __
		dé -- ja -- "lo es" -- tar. __
		Ca -- da dí -- a __
		tie -- ne bas -- tan -- te con "su in" -- quie -- tud, __
		vi -- ve tu vi -- da, __
		siem -- pre mi -- ran -- "do a" la luz. __

		Llé -- va -- la con -- ti -- go siem -- pre, __
		no mi -- res a -- trás.
		Y cuan -- "do a" ve -- ces sien -- tas __
		nos -- tal -- gia "y os" -- cu -- ri -- dad, __
		pien -- sa que al -- guien __
		tie -- ne tu mis -- "mo i" -- de -- al. __

		Si -- gue la luz. __
	}
>>
