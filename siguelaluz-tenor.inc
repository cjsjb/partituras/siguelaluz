\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		\key g \major

		R1*2  |
		r4 d 8 d 4 d e 8 ~  |
		e 8 d 4 d 8 ~ d 2  |
%% 5
		r4 cis 8 cis cis b, 4 cis 8 ~  |
		cis 1  |
		r4 g 8 g 4 g a 8 ~  |
		a 8 g 4 g 8 ~ g 2  |
		r4 g 8 g 4 g a 8 ~  |
%% 10
		a 8 g 4 g 8 ~ g 2  |
		r4 g 8 g 4 g a 8 ~  |
		a 8 g 4 fis ( g 4. )  |
		a 1 ~  |
		a 1  |
%% 15
		r4 d 8 d 4 d e 8 ~  |
		e 8 d 4 d 8 ~ d 2  |
		r4 cis 8 cis cis b, 4 cis 8 ~  |
		cis 1  |
		r4 g 8 g 4 g a 8 ~  |
%% 20
		a 8 g 4 g 8 ~ g 2  |
		r4 g 8 g 4 g a 8 ~  |
		a 8 g 4 g 8 ~ g 2  |
		r4 g 8 g 4 g a 8 ~  |
		a 8 g 4 fis ( g 4. )  |
%% 25
		a 1  |
		r8 c' c' c' c' c' c' c' ~  |
		c' 4. d' 8 ~ d' 2 ~  |
		d' 8 r r d' 4 d' 8 d' 4  |
		dis' 1 ~  |
%% 30
		dis' 8 r r b 4 ( a 8 b ) a  |
		a 4. g 8 ~ g 2  |
		r8 g a g a a 4 g 8  |
		a 4 b 8 g ~ g 2 ~  |
		g 2 r8 b b b  |
%% 35
		b 4. a 8 ~ a 4. r8  |
		r8 e b a 16 b 8. a 8 b d' ~  |
		d' 2.. r8  |
		r8 c' 4 c' 8 c' c' c' c'  |
		c' 4. d' 8 ~ d' 2 ~  |
%% 40
		d' 8 r r d' d' 8. d' d' 8 ~  |
		d' 8 dis' 2. r8  |
		r8 a b a b a ( b 4 )  |
		a 4. g 8 ~ g 2  |
		r8 g a g a g a g ~  |
%% 45
		g 1 ~  |
		g 4. r8 r b b b  |
		b 4. a 8 ~ a 2  |
		r8 e b a 16 b 8. a 8 b d' ~  |
		d' 1 ~  |
%% 50
		d' 2 r  |
		r4 d 8 d 4 d e 8 ~  |
		e 8 d 4 d 8 ~ d 2  |
		r4 cis 8 cis cis b, 4 cis 8 ~  |
		cis 1  |
%% 55
		r4 g 8 g 4 g a 8 ~  |
		a 8 g 4 g 8 ~ g 2  |
		r4 g 8 g 4 g a 8 ~  |
		a 8 g 4 g 8 ~ g 2  |
		r4 g 8 g 4 g a 8 ~  |
%% 60
		a 8 g 4 fis g 4.  |
		a 1 ~  |
		a 1  |
		r4 d 8 d 4 d e 8 ~  |
		e 8 d 4 d 8 ~ d 2  |
%% 65
		r4 cis 8 cis cis b, 4 cis 8 ~  |
		cis 1  |
		r4 g 8 g 4 g a 8 ~  |
		a 8 g 4 g 8 ~ g 2  |
		r4 g 8 g 4 g a 8 ~  |
%% 70
		a 8 g 4 g 8 ~ g 2  |
		r4 g 8 g 4 g a 8 ~  |
		a 8 g 4 fis ( g 4. )  |
		a 1  |
		r8 c' c' c' c' c' c' c' ~  |
%% 75
		c' 4. d' 8 ~ d' 2 ~  |
		d' 8 r r d' 4 d' 8 d' 4  |
		dis' 1 ~  |
		dis' 8 r r b 4 ( a 8 b ) a  |
		a 4. g 8 ~ g 2  |
%% 80
		r8 g a g a a 4 g 8  |
		a 4 b 8 g ~ g 2 ~  |
		g 2 r8 b b b  |
		b 4. a 8 ~ a 4. r8  |
		r8 e b a 16 b 8. a 8 b d' ~  |
%% 85
		d' 2.. r8  |
		r8 c' 4 c' 8 c' c' c' c'  |
		c' 4. d' 8 ~ d' 2 ~  |
		d' 8 r r d' d' 8. d' d' 8 ~  |
		d' 8 dis' 2. r8  |
%% 90
		r8 a b a b a ( b 4 )  |
		a 4. g 8 ~ g 2  |
		r8 g a g a g a g ~  |
		g 1 ~  |
		g 4. r8 r b b b  |
%% 95
		b 4. a 8 ~ a 2  |
		r8 e b a 16 b 8. a 8 b d' ~  |
		d' 1 ~  |
		d' 8 r r4 a 8 g fis g ~  |
		g 1  |
%% 100
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Dios "te ha es" -- co -- gi -- do hoy, __
		tie -- nes que par -- tir. __
		Dios "te ha es" -- co -- gi -- do hoy, __
		de -- ja tu ca -- "sa y" ven. __
		Dios "te ha es" -- co -- gi -- do hoy, ven. __

		Dios "te ha es" -- co -- gi -- do hoy, __
		tie -- nes que llo -- rar. __
		Dios "te ha es" -- co -- gi -- do hoy, __
		tam -- bién de -- bes re -- ír. __
		Dios "te ha es" -- co -- gi -- do hoy, ven.

		No pien -- ses en el ma -- ña -- na, __
		dé -- ja -- "lo es" -- tar. __
		Ca -- da dí -- a __
		tie -- ne bas -- tan -- te con "su in" -- quie -- tud, __
		vi -- ve tu vi -- da, __
		siem -- pre mi -- ran -- "do a" la luz. __

		Llé -- va -- la con -- ti -- go siem -- pre, __
		no mi -- res a -- trás.
		Y cuan -- "do a" ve -- ces sien -- tas __
		nos -- tal -- gia "y os" -- cu -- ri -- dad, __
		pien -- sa que al -- guien __
		tie -- ne tu mis -- "mo i" -- de -- al. __

		Si -- "gue a" -- de -- lan -- te fiel, __
		a ve -- ces cae -- rás. __
		Lu -- cha con gran te -- són, __
		vuél -- ve -- "te a" le -- van -- tar, __
		lu -- "cha en" -- car -- ni -- za -- da ten -- drás. __

		Dios "te ha es" -- co -- gi -- do hoy, __ 
		tie -- nes que par -- tir. __
		Dios "te ha es" -- co -- gi -- do hoy, __
		de -- ja tu ca -- "sa y" ven. __
		Dios "te ha es" -- co -- gi -- do hoy, ven.

		No pien -- ses en el ma -- ña -- na, __
		dé -- ja -- "lo es" -- tar. __
		Ca -- da dí -- a __
		tie -- ne bas -- tan -- te con "su in" -- quie -- tud, __
		vi -- ve tu vi -- da, __
		siem -- pre mi -- ran -- "do a" la luz. __

		Llé -- va -- la con -- ti -- go siem -- pre, __
		no mi -- res a -- trás.
		Y cuan -- "do a" ve -- ces sien -- tas __
		nos -- tal -- gia "y os" -- cu -- ri -- dad, __
		pien -- sa que al -- guien __
		tie -- ne tu mis -- "mo i" -- de -- al. __

		Si -- gue la luz. __
	}
>>
